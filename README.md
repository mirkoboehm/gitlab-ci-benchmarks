# Gitlab CI Performance Benchmarks

The benchmark consists of a set of binary data files stored in Git-LFS
and a CI configuration that calculates and verifies the MD5 sums of
the data files in a pipeline. The goal is to measure the time it takes
to execute the whole pipeline and to verify the checksums.

## Random statistics

### Upload durations

After creating the repository and adding 10 data files of 1GB random
content each, these are the upload durations measured using `time git
push <remote> main`. 

* Corporate VPN, corporate-hosted Gitlab: 

```
> Uploading LFS objects: 100% (10/10), 11 GB | 3.6 MB/s, done.
> ...
> real	49m42.815s
```

* No VPN, upload to gitlab.com:

Did not succeed, exceeds storage limit on free tier.
